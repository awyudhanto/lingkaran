# Calculate Circumference
## Objectives
Memahami struktur alur program dengan algoritma
Dapat membuat algoritma dari sebuah permasalahan kecil
## Directions
Kamu diminta untuk membuat sebuah algoritma untuk menghitung keliling dan luas sebuah lingkaran dimana hasil perhitungan akan ditampilkan.

Secara matematis rumus keliling lingkaran adalah 2 x pi x jari-jari
Secara matematis rumus luas lingkaran adalah pi x jari-jari ^ 2
